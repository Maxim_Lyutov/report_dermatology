%include('./templates/header.tpl', style='../static/styles.css', username=username)

<p>Добавить новые данные:</p>
<form action="/new" method="GET">

<input type="text" size="50" maxlength="50" name="doctor" placeholder="Кто сдает"><br>
<input type="email" size="50" maxlength="50" value="{{username}}" name="email" placeholder="Почта"><br>

<select name="region" placeholder="Район">
%for r in regions:
        <option value="{{r[0]}}"> {{r[1]}} </option> 
%end
</select><br>

<select name="status" placeholder="Статус">
%for s in statuses:
    <option value="{{s[0]}}">{{s[1]}}</option>
%end 
</select><br>

<br>
<input type="submit" name="save" value="Добавить">
</form>

%include('./templates/footer.tpl')