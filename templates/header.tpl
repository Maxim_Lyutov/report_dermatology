<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> {{title}} </title>
        <link href="{{style}}" rel="stylesheet">
    </head>
    <body>
        <div id="header">
            <h2 align="center" >Годовая отчетность</h2>
        </div>
        %if username :
        <div id="footer" >Имя : {{username}}</div>
        <div id="menu">
            <ul class="nav">
                <li><a href="/new">Добавить</a></li>
                <li><a href="/list">Список</a></li>
                <li><a href="/help">Справка</a></li>
                <li><a href="/exit">Выход</a></li>
            </ul>
        </div>
        %end

        <div id="main">
