%include('./templates/header.tpl', style='../static/styles.css',access=True)

<p> Редактирование : </p>
<form action="/edit/{{nomer}}" method="get">

<label>Кто сдает </label>
<input type="text" name="doctor" value="{{row[1]}}" size="50" maxlength="100">
<br>
<label>Почта</label>
<input type="text" name="email" value="{{row[2]}}" size="50" maxlength="100">
<br>

<label>Район</label>
<select name="region" placeholder="Район">

%for r in regions:
        %if row[5] != r[0]:
            <option value="{{r[0]}}"> {{r[1]}} </option>
        %else:
            <option selected value="{{r[0]}}"> {{r[1]}} </option>
        %end 
%end
</select><br>

<label>Статус</label>
<select name="status" placeholder="Статус">
%for s in statuses:
    %if row[3] != s[0]:
        <option value="{{s[0]}}">{{s[1]}}</option>
    %else:
        <option selected value="{{s[0]}}">{{s[1]}}</option>
    %end
%end 
</select>
<br>
<table>
%for k in obj:
    <tr>
        <td>{{k}}</td> 
        <td><input type="text" name="{{k}}" value="{{obj[k]}}" size="5" maxlength="5" </td>
    </tr>
%end
</table>
<br>
<input type="submit" name="save" value="Сохранить">
</form>

%include('./templates/footer.tpl')
