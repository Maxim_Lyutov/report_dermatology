%include('./templates/header.tpl', style='../static/styles.css', username=username)

<table>
<caption> Отчет {{year}}</caption>
<tr>
    <th>Номер </th>
    <th>Ответственный </th>
    <th>Почта </th>
    <th> % </th>
    <th>Район </th>
    <th>Дата </th>
    <th>Правка</th>
</tr>
%i = 1
%for row in rows:
    <tr>
    <td>{{i}}</td>
    <td>{{row[1]}}</td>
    <td>{{row[2]}}</td>
    <td>{{row[3]}}</td>
    <td>{{row[4]}}</td>
    <td>{{row[5]}}</td>
    
    <td> <a href="/edit/{{row[0]}}">Изменить </a> </td>
    </tr>
    %i = i + 1
%end
</table>
<br>
<div align="center" >
%for y in range(now - 1,now + 2):
    <a href="/list/{{y}}"> [{{y}}] </a>
%end
</div>
%include('./templates/footer.tpl')