import model, json, datetime
from bottle import route, run, debug, error, template, request, static_file, redirect, response

@route('/static/<filename>')
def server_static(filename):
    return static_file(filename, root='./static/')

@error(404)
def error404(error):
    return 'Ошибка 404 <br>  Ресурс не найден ! <br><a href="/" > На главную страницу</a>'

@error(403)
@error(405)
def mistake(code):
    return 'Переданный вами параметр имеет неправильный формат!<br><a href="/" > На главную страницу</a>'

@route('/')
@route('/index')
def index():
    return template('templates/start', title = 'Начало')

@route('/list')
@route('/list/<year:int>')
def list_doctor(year = datetime.datetime.now().year):
    login = request.cookies.get("account")
    if login is not None:
        if 1 == model.getone(model.get_role, login)[0] : # admin role !?
            res = model.getreport(model.list_all, year)
        else:
            res = model.getreport(model.doctor_only, year, login )
        view = template('templates/list', rows = res, title = 'Данные по районам' ,year = year ,now = datetime.datetime.now().year , username = login)
        return view
    else :
        return redirect('/login')

@route('/new', method='GET')
def new_item():
    if request.cookies.get("account") is not None:
        if request.GET.get('save','').strip():
            doctor = request.query.doctor.strip()
            email = request.GET.get('email','').strip()
            status = request.GET.get('status','').strip()
            region = request.GET.get('region','').strip()
            
            model.new(doctor, email ,status, region)
            return  redirect('/list')
        else:
            region = model.getall(model.region_all)
            status = model.getall(model.status_all)
            return template('templates/new',title = 'Добавление нового отчета', regions = region, statuses = status, username = request.cookies.get("account")) 
    else: redirect('/login')

@route('/edit/<nomer:int>', method='GET')
def edit_item(nomer):
    if request.cookies.get("account") is not None:

        if request.GET.get('save','').strip():
            doctor = request.query.doctor.strip()
            status = request.GET.get('status','').strip()
            region = request.GET.get('region','').strip()
            email = request.GET.get('email','').strip()

            model.update(doctor, status, region, email,nomer)
            return redirect('/list')
        else:
            cur_data = model.getone(model.doctor_id, nomer)

            region = model.getall(model.region_all)
            status = model.getall(model.status_all)
            try:
                data = model.getone(model.region_one, nomer)[0]
            except :
                data ='{}'

            return template('templates/edit_one', row=cur_data, regions=region, statuses=status, nomer=nomer, title='Правка данных', obj = json.loads(data), username = request.cookies.get("account"))
    else : redirect('/login')

@route ('/reg', method =['GET','POST'])
def registration():
    if request.forms.get('regist',''):
        email = request.forms.get('email','')
        region = request.forms.get('region','')
        
        if model.send_link(email,region, 'http://'+request.get_header('host')):
            return '<h3>Информация ! </h3><h4>На почту было отправлено письмо активации доступа </h4><a href="/login"> Вход </a>'
        else:
            return '<h3>Ошибка  </h3><h4> Не возможно создать доступ ...</h4><h4><a href="/reg"> Попробовать еще раз ? </a></h4>'
    else:
        region = model.getall(model.region_all)
        return template('templates/regist', title ='Регистрация пользователя', regions = region )

@route('/active/<active_id>')
def active_login(active_id):
    model.activation(active_id)
    return template("./templates/login", title ='Вход',msg = 'Введите активированный доступ')

@route ('/login', method =['GET','POST'])
def login():
    text = ''
    if request.forms.get('login',''):
        username = request.forms.get('username')
        password = request.forms.get('password')
        access, text = model.check_login(username, password)
        
        if access :
            response.set_cookie("account", username, max_age=3600)
            return redirect('/list')
        
    return template("templates/login", title ='Вход',msg = text)

@route('/exit')
def exit():
    response.delete_cookie("account")
    return redirect('/')

@route('/help')
def help():
    return template("templates/help", title = 'Справка' , username = request.cookies.get("account") )

#-------------------------------------------------------------------------------
debug(True)
run(host='localhost', port='9000', reloader=True)

#import cProfile	
#cProfile.run("run(host='localhost', port='9000', reloader=True)")