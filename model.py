import sqlite3, smtplib, random
region_all = "select t1.id, t1.region from region as t1 order by t1.region"
status_all = "select t1.id, t1.status from status as t1 order by t1.status"
region_one = "select data, id_doctor from reports where id_doctor = '%s'"
get_role = "select users.role from users where users.email = '%s'"
doctor_id = '''
    select 
        doctor.id,
        name_doctor,
        email,
        status.id,
        status,
        region.id,
        region,
        date_report
    from doctor join status 
    on status.id = doctor.status_id 
        join region 
    on region.id = doctor.region_id
    where doctor.id = %d
    '''
list_all ='''
    select 
        doctor.id,
        name_doctor,
        doctor.email,
        status,
        region,
        date_report 
    from doctor 
        inner join status 
    on status.id = doctor.status_id 
        inner join region 
    on region.id = doctor.region_id
    '''

doctor_only ='''
    select 
        doctor.id,
        name_doctor,
        doctor.email,
        status,
        region,
        date_report 
    from doctor 
        inner join status 
    on status.id = doctor.status_id 
        inner join region 
    on region.id = doctor.region_id
    where doctor.email = '%s' 
    and ( date_report >= '%s' and date_report <= '%s')
    '''

def init():
    con = sqlite3.connect('report_derm.db')
    cur = con.cursor()
    return con,cur

def getone(query1 ,nomer):
    con,cur = init()
    cur.execute(query1  % (nomer))
    res = cur.fetchone()
    con.close()
    return res

def getall(query1):
    con,cur = init()
    cur.execute(query1)
    res = cur.fetchall()
    cur.close()
    con.close()
    return res

def getreport(query1, year = 2020, login = 'admin' ):
    con,cur = init()

    if login == 'admin':
        cur.execute(query1 + " where ( date_report >= '%s' and date_report <= '%s')" % ( str(year)+'-01-01', str(year)+'-12-31'))
    else:
        cur.execute(query1 % ( login, str(year)+'-01-01', str(year)+'-12-31'))
    
    res = cur.fetchall()
    cur.close()
    con.close()
    return res

def new(doctor, email ,status, region):
    con,cur = init()
    query2= "insert into doctor ( name_doctor, email, status_id, region_id, date_report) values('%s','%s','%s','%s', date('now')) "  % (doctor, email ,status, region)
    cur.execute(query2)
    con.commit()
    cur.close()
    return 

def update(doctor, status, region, email, nomer):
    con,cur = init()
    query2 = "update doctor set name_doctor='%s' , status_id='%s', region_id='%s' , email='%s'  where doctor.id like '%d'"  % (doctor, status, region, email, nomer)
    cur.execute(query2)
    con.commit()
    cur.close()
    return

def check_login(username, password):
    result = [False,'Не верная пара: логин/пароль ']

    access = getall("select email, pass, active from users")
    for row in access:
        if row[0] == username and row[1] == password :  
            if row[2] == 'True':
                result[0] = True
                result[1] =''
            else:
                result[1] = 'Проверьте почту чтобы активировать доступ...'
    return result

def activation(active_id):
    con,cur = init()
    query2 = "update users set active='True'  where code like '%s'"  % (active_id)
    cur.execute(query2)
    con.commit()
    cur.close()
    return 

def insert_login(email, region_id):
    code = ''.join(random.sample('qwertyuiopasdfghjklzxcvbnmASDFGHJKLZXCVBNMQWERTYUIOP',28))
    active = 'False'
    pwd = ''.join(random.sample('qwert123456789yasdfghjzxvbnmASDFGHJKLZXVNQWRTYUP',8))
    
    query2 = "insert into users (code, email, region_id, pass, active, role ) values('%s','%s','%s','%s','%s','%s' )"  % (code, email, region_id, pwd, active, '2')
    
    con,cur = init()
    try:
        cur.execute(query2)
        con.commit()
    except :
        return None,email, pwd
    cur.close()

    return code,email,pwd

def send_link(email, region, host):
    code,log,pwd = insert_login(email, region)

    if code is not None:
        txt_link = host + '/active/' + code

        connect = {
            'name_smtp':'smtp.gmail.com',
            'from' : '@gmail.com',
            'pwd' : '',
            'to' : email,
            'subj' : 'Активация доступа для подачи годовой отчетности',
            'msg_body' : 'Логин: %s \nПароль: %s\nДля регистрации доступа пройдите по ссылке : %s\nЕсли вы не регистрировались, то проигнорируйте это письмо' % (log, pwd, txt_link),
        }

        srv = smtplib.SMTP_SSL(connect['name_smtp'],465)
        srv.login(connect['from'], connect['pwd'])
        msg = "From: %s\nTo: %s\nSubject: %s\n\n%s"  % (connect['from'], connect['to'], connect['subj'], connect['msg_body'])

        srv.sendmail(connect['from'], connect['to'], format(msg).encode('utf-8'))
        srv.quit()
        return True

    return None